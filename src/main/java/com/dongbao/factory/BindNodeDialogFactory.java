package com.dongbao.factory;

import com.dongbao.dialog.BindNodeDialog;
import com.intellij.openapi.actionSystem.AnActionEvent;

public class BindNodeDialogFactory {

    public static BindNodeDialog getInstance(AnActionEvent event){
        BindNodeDialog patcherDialog = new BindNodeDialog(event);
        return patcherDialog;
    }

    public static BindNodeDialog getBindNode2Instance(AnActionEvent event){
        BindNodeDialog patcherDialog = new BindNodeDialog(event);
        return patcherDialog;
    }

}
