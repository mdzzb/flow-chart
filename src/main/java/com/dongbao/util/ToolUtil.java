package com.dongbao.util;

import com.intellij.psi.PsiMethod;
import org.apache.commons.lang3.StringUtils;
import org.xmind.core.INotes;
import org.xmind.core.IPlainNotesContent;
import org.xmind.core.ITopic;
import org.xmind.core.IWorkbook;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ToolUtil {
    /**
     * 拷贝到剪贴板
     *
     * @param text 文本
     * @author Kings
     * @date 2021/06/07
     */
    public static void setClipboardString(String text) {
        // 获取系统剪贴板
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        // 封装文本内容
        Transferable trans = new StringSelection(text);
        // 把文本内容设置到系统剪贴板
        clipboard.setContents(trans, null);
    }
    /**
     * 检测文本中是否有中文汉子存在
     *
     */
    public static boolean isContainChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;

    }
    /**
     * 创建思维导图中的content中的信息
     *
     */
    public static void createMethodInfo(ITopic iTopic, PsiMethod method, IWorkbook workbook) {
        if(method!=null){
            StringBuffer sb = new StringBuffer();
            sb.append("类: "+method.getContainingClass().getName());
            sb.append("\n");
            sb.append("方法:"+method.getName());
            sb.append("\n");
            createNotes(iTopic,sb.toString(),workbook);
        }
    }
    public static void createNotes(ITopic iTopic,String info,IWorkbook workbook){
    if (StringUtils.isNotEmpty(info)) {
            INotes notes = iTopic.getNotes();
            IPlainNotesContent plainContent = (IPlainNotesContent) workbook.createNotesContent(INotes.PLAIN);
            plainContent.setTextContent(info);
            notes.setContent(INotes.PLAIN, plainContent);
        }
    }
}
