package com.dongbao.util;


import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.io.ByteArrayInputStream;
import java.util.*;

/**
 * <p>XML转map工具类</p>
 *
 * @version 1.0
 * @author:hewei
 * @date: 2019-09-20 23:08
 * @history
 */
public class XmlParseUtils {


    /**
     * xml 转map
     *
     * @param str
     * @return
     */
    public static Map<String, Object> StrToMap(String str) {
        try {
            Document doc = DocumentHelper.parseText(str);
            Map<String, Object> map = XmlParseUtils.DomToMap(doc);
            return map;
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param doc
     * @return
     */

    public static Map<String, Object> DomToMap(Document doc) {

        Map<String, Object> map = new HashMap<String, Object>();

        if (doc == null) {
            return map;
        }

        Element root = doc.getRootElement();

        for (Iterator iterator = root.elementIterator(); iterator.hasNext(); ) {

            Element e = (Element) iterator.next();

            List list = e.elements();

            if (list.size() > 0) {

                map.put(e.getName(), DomToMap(e));

            } else {
                map.put(e.getName(), e.getText());
            }

        }

        return map;

    }

    @SuppressWarnings("unchecked")

    public static Map DomToMap(Element e) {

        Map map = new HashMap();

        List list = e.elements();

        if (list.size() > 0) {

            for (int i = 0; i < list.size(); i++) {

                Element iter = (Element) list.get(i);

                List mapList = new ArrayList();


                if (iter.elements().size() > 0) {

                    Map m = DomToMap(iter);

                    if (map.get(iter.getName()) != null) {

                        Object obj = map.get(iter.getName());

                        if (!obj.getClass().getName().equals("java.util.ArrayList")) {

                            mapList = new ArrayList();

                            mapList.add(obj);

                            mapList.add(m);

                        }

                        if (obj.getClass().getName().equals("java.util.ArrayList")) {

                            mapList = (List) obj;

                            mapList.add(m);

                        }

                        map.put(iter.getName(), mapList);

                    } else {
                        map.put(iter.getName(), m);
                    }

                } else {

                    if (map.get(iter.getName()) != null) {

                        Object obj = map.get(iter.getName());

                        if (!obj.getClass().getName().equals("java.util.ArrayList")) {

                            mapList = new ArrayList();

                            mapList.add(obj);

                            mapList.add(iter.getText());

                        }

                        if (obj.getClass().getName().equals("java.util.ArrayList")) {

                            mapList = (List) obj;

                            mapList.add(iter.getText());

                        }

                        map.put(iter.getName(), mapList);

                    } else {
                        map.put(iter.getName(), iter.getText());
                    }

                }

            }

        } else {
            map.put(e.getName(), e.getText());
        }
        return map;

    }


//    /**
//     * @param <T>
//     * @param xml         XML字符串
//     * @param elementName 对象XML根元素的名称
//     * @param cls         返回类型
//     *
//     * @return
//     *
//     * @author : zKF27092
//     * @version: 1.0
//     * 时间 : 2010-12-15
//     * <p>
//     * 描述 : 将XML字符串转换成对象
//     * <p>
//     * 实现方法：将XML字符串转换成对象
//     * <p>
//     * Copyright 1988-2005, Huawei Tech. Co., Ltd.
//     */
//    @SuppressWarnings("unchecked")
//    public static <T> T xmlToBean(String xml, String elementName, Class<T> cls) {
//        T object = null;
//        try {
//            Document document = DocumentHelper.parseText(xml);
//            //如果不是SOAP返回的报文，是XML字符串则不需要这行代码
//            String beanXml = document.getRootElement().element("Body").element(elementName).asXML();
//
//            OMElement omElement = new StAXOMBuilder(new ByteArrayInputStream(beanXml.getBytes("UTF-8"))).getDocumentElement();
//
//            object = (T) BeanUtil.processObject(omElement, cls, null, true, new DefaultObjectSupplier(), (Type) null);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return object;
//    }


    /**
     * 解析xml文件返回保存cls的List集合，如果返回码resultCode=1时则返回List为null
     *
     * @param xml
     * @param cls
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static List<?> xmlToMap(String xml, Class<?> cls) throws Exception {
        List<Object> lists = new ArrayList<>();
        Document doc = DocumentHelper.parseText(xml);
        Element et = doc.getRootElement();

        final Map map = Dom2Map(et);
        lists.add(map);
        return lists;
    }

    @SuppressWarnings("unchecked")
    public static Map Dom2Map(Element e) {
        Map map = new HashMap();
        List list = e.elements();
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Element iter = (Element) list.get(i);
                List mapList = new ArrayList();

                if (iter.elements().size() > 0) {
                    Map m = Dom2Map(iter);
                    if (map.get(iter.getName()) != null) {
                        Object obj = map.get(iter.getName());
                        if (!obj.getClass().getName().equals("java.util.ArrayList")) {
                            mapList = new ArrayList();
                            mapList.add(obj);
                            mapList.add(m);
                        }
                        if (obj.getClass().getName().equals("java.util.ArrayList")) {
                            mapList = (List) obj;
                            mapList.add(m);
                        }
                        map.put(iter.getName(), mapList);
                    } else {
                        map.put(iter.getName(), m);
                    }
                } else {
                    if (map.get(iter.getName()) != null) {
                        Object obj = map.get(iter.getName());
                        if (!obj.getClass().getName().equals("java.util.ArrayList")) {
                            mapList = new ArrayList();
                            mapList.add(obj);
                            mapList.add(iter.getText());
                        }
                        if (obj.getClass().getName().equals("java.util.ArrayList")) {
                            mapList = (List) obj;
                            mapList.add(iter.getText());
                        }
                        map.put(iter.getName(), mapList);
                    } else {
                        map.put(iter.getName(), iter.getText());
                    }
                }
            }
        } else {
            map.put(e.getName(), e.getText());
        }
        return map;
    }


}
