package com.dongbao.util;

import cn.hutool.core.thread.ThreadUtil;
import com.intellij.ide.DataManager;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.ui.popup.JBPopupListener;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.tree.PsiCommentImpl;
import com.intellij.psi.impl.source.tree.PsiWhiteSpaceImpl;
import com.intellij.psi.impl.source.tree.java.PsiJavaTokenImpl;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.dongbao.common.AuthorityEnum.AUTHORITY_NOTIFICATION_TITLE;

public class IdeaApiUtil {



    public static void showBalloon(JComponent jComponent, String title, JBPopupListener jbPopupListener, Editor editor) {
        IdeaApiUtil.showBalloon(jComponent, title, jbPopupListener, true, true, editor);
    }


    public static void showBalloon(JComponent jComponent, String title, JBPopupListener jbPopupListener, boolean hideOnClickOutside, boolean dialogMode, Editor editor) {
        final JBPopupFactory popupFactory = JBPopupFactory.getInstance();
        final Balloon balloon = popupFactory.createDialogBalloonBuilder(jComponent, title)
                .setHideOnClickOutside(hideOnClickOutside)
                .setDialogMode(dialogMode)
                .createBalloon();
        balloon.addListener(jbPopupListener);
        balloon.show(popupFactory.guessBestPopupLocation(editor), Balloon.Position.below);
    }

    public static Integer getSelLineNumber(@NotNull Editor editor) {
        return editor.getDocument().getLineNumber(editor.getCaretModel().getOffset());
    }

    public static byte[] getBytes(@NotNull VirtualFile virtualFile) throws IOException {
        InputStream inputStream = virtualFile.getInputStream();
        byte[] bytes = new byte[inputStream.available()];
        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)) {
            bufferedInputStream.read(bytes);
        }
        return bytes;
    }


    /**
     * 通知
     *
     * @param content     通知内容
     * @param type warning,info,error
     */

    public static void showNotification(String content, NotificationType type, Project project) {
        showNotification(content,type,project,"DHG代码提醒");
    }

    public static void showNotification(String content, NotificationType type, Project project,String title) {
        //BALLOON：自动消失 NotificationGroup
        Notification notification = new Notification(AUTHORITY_NOTIFICATION_TITLE, title, content, type);
        ThreadUtil.execute(()->{
            Notifications.Bus.notify(notification, project);
        });
    }

    public static void showErrNotification(String content, Project project) {
        showNotification(content, NotificationType.ERROR, project);
    }

    public static void showInfoNotification(String content, Project project) {
        showNotification(content, NotificationType.INFORMATION, project);
    }


    /**
     * 颜色选择
     * @param placement 选择器放置位置
     * @param lc 需要监听的组件
     */
    public static void chooseColorListener(JComponent placement, JComponent lc) {
        lc.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                final Color chooseColor = JColorChooser.showDialog(
                        placement, "Choose color", lc.getForeground());

                if (null != chooseColor)
                    lc.setForeground(chooseColor);
            }
        });
    }

    /**
     * 当前打开的编辑器跳转到指定行号
     * */
    public static boolean gotoLine(int lineNumber,Project project) {

        DataContext dataContext = DataManager.getInstance().getDataContext();

        Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();

        if( editor == null )
            return false;

        CaretModel caretModel = editor.getCaretModel();
        int totalLineCount = editor.getDocument().getLineCount();

        if( lineNumber > totalLineCount )
            return false;

        //Moving caret to line number
        caretModel.moveToLogicalPosition(new LogicalPosition(lineNumber-1,0));

        //Scroll to the caret
        ScrollingModel scrollingModel = editor.getScrollingModel();
        scrollingModel.scrollToCaret(ScrollType.CENTER);

        return true;
    }

    /**
     * 判断节点是否是需要省略的节点 类似括号、换行、注释节点
     * */
    public static Boolean isOmitElement(PsiElement element){
        if(element instanceof PsiJavaTokenImpl){
            //大括号节点
            return true;
        }
        if(element instanceof PsiWhiteSpaceImpl){
            //回车换行节点
            return true;
        }
        if(element instanceof PsiCommentImpl){
            //注释节点
            return true;
        }
        return false;
    }


}
