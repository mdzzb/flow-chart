package com.dongbao.util;

import com.dongbao.common.AuthorityEnum;
import com.intellij.notification.NotificationDisplayType;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;

public class TipUtil {

    private static NotificationGroup notificationGroup = new NotificationGroup(AuthorityEnum.AUTHORITY_NOTIFICATION_TITLE, NotificationDisplayType.BALLOON,true);

    public static void sendNotification(String title, String message, NotificationType notificationType){
        Notifications.Bus.notify(notificationGroup.createNotification(message,notificationType));

    }
}
