package com.dongbao.util;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Disposer;
import com.intellij.openapi.util.ThrowableComputable;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.impl.PsiManagerImpl;
import com.intellij.psi.impl.file.impl.FileManager;
import com.intellij.util.WaitFor;
import com.intellij.util.io.ReadOnlyAttributeUtil;

import java.io.IOException;

public class FileUtil {

    private static final Disposable myDisposable = Disposer.newDisposable();

    /**
     * 设置文件只读状态
     * */
    public static void makeFileReadOnly(Project myProject, VirtualFile file) {

        try {
            ReadOnlyAttributeUtil.setReadOnlyAttribute(file, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Disposable getTestRootDisposable() {
        return myDisposable;
    }

}
