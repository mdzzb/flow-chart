package com.dongbao.util;

public class StorageConfig {

    /**
     * if是否分开层级
     * */
    private Boolean ifPartLevel;

    /**
     * 导出思维导出的结构图
     * */
    private String structure;

    public Boolean getIfPartLevel() {
        return ifPartLevel;
    }

    public void setIfPartLevel(Boolean ifPartLevel) {
        this.ifPartLevel = ifPartLevel;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

}
