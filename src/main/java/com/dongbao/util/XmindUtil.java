package com.dongbao.util;

import org.xmind.core.ITopic;
import org.xmind.core.internal.dom.TopicImpl;

public class XmindUtil {

    public static void mergeChild(ITopic original, ITopic newTopic){
        for(ITopic orgChild:original.getAllChildren()){
            ITopic newChild = newTopic.getOwnedWorkbook().createTopic();
            newChild.setTitleText(orgChild.getTitleText());
            newTopic.add(newChild);
            if(orgChild.getAllChildren().size()>0){
                mergeChild(orgChild,newTopic);
            }
        }
    }

}
