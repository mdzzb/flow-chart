package com.dongbao.common;

import com.intellij.psi.PsiClass;
import com.intellij.psi.impl.source.tree.PsiCommentImpl;
import lombok.Data;
import org.xmind.core.IWorkbook;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 生成脑图参数
 * @author: lirufei
 * @date: 2023/4/18 12:49
 * @param: null
 * @return
 */
@Data
public class AnalysisParam {
  // 生成脑图工作表
  private IWorkbook workbook;
  // 方法所在类
  private PsiClass target;
  // 类所包含的泛型类
  private List<PsiClass> genericClassList;
  // 已经使用过的注释对象
  private List<PsiCommentImpl> usedConnentList;

  private List<Integer> comments = new ArrayList<>();
  // 转换类型 1 If语句 2 for语句 3 while语句
  private int type;
  //只有if的语句，没有else 和 else if
  private boolean onceIf;

  public AnalysisParam() {
    super();
  }

  public AnalysisParam(
      IWorkbook workbook,
      PsiClass target,
      List<PsiClass> genericClassList,
      List<PsiCommentImpl> usedConnentList,
      int type) {
    this.workbook = workbook;
    this.target = target;
    this.genericClassList = genericClassList;
    this.usedConnentList = usedConnentList;
    this.type = type;
  }

  public IWorkbook getWorkbook() {
    return workbook;
  }

  public void setWorkbook(IWorkbook workbook) {
    this.workbook = workbook;
  }

  public PsiClass getTarget() {
    return target;
  }

  public void setTarget(PsiClass target) {
    this.target = target;
  }

  public List<PsiClass> getGenericClassList() {
    return genericClassList;
  }

  public void setGenericClassList(List<PsiClass> genericClassList) {
    this.genericClassList = genericClassList;
  }

  public List<PsiCommentImpl> getUsedConnentList() {
    return usedConnentList;
  }

  public void setUsedConnentList(List<PsiCommentImpl> usedConnentList) {
    this.usedConnentList = usedConnentList;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public List<Integer> getComments() {
    return comments;
  }

  public void setComments(List<Integer> comments) {
    this.comments = comments;
  }

  public boolean isOnceIf() {
    return onceIf;
  }

  public void setOnceIf(boolean onceIf) {
    this.onceIf = onceIf;
  }
}
