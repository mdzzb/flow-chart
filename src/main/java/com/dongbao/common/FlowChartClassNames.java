package com.dongbao.common;

import com.intellij.util.containers.ContainerUtil;
import org.jetbrains.annotations.NonNls;

import java.util.List;

public interface FlowChartClassNames {

  @NonNls String ACCESSORS = "lombok.experimental.Accessors";

  List<String> MAIN_LOMBOK_CLASSES = ContainerUtil.immutableList(ACCESSORS);

}
