package com.dongbao.common;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public interface PluginIcons {

    Icon fastRequest_toolwindow = IconLoader.getIcon("/icon/fastRequest_toolwindow.svg", PluginIcons.class);

    Icon dhgplug_toolwindow = IconLoader.getIcon("/pluginIcon.svg", PluginIcons.class);

}
