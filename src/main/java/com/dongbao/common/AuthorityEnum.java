package com.dongbao.common;

public class AuthorityEnum {

    public static final String AUTHORITY_NOTIFICATION_TITLE = "flow-chart";

    /**
     * 注解所在的包名
     * */
    public static final String MAIN_ANNOTIONS_NAME = "com.flow.annotations.FlowChart";

    /**
     * 子节点注解
     * */
    public static final String CHILD_ANNOTIONS_NAME = "com.flow.annotations.FlowChild";

}
