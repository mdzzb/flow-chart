package com.dongbao.action;

import com.dongbao.common.AuthorityEnum;
import com.dongbao.factory.BindNodeDialogFactory;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.PsiMethodImpl;
import com.intellij.psi.impl.source.tree.java.PsiAnnotationImpl;
import org.jetbrains.annotations.NotNull;

import static com.dongbao.common.AuthorityEnum.MAIN_ANNOTIONS_NAME;

public class BindNodeAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        // 打开配置弹窗
        BindNodeDialogFactory.getBindNode2Instance(e);

    }



    /**
     * 只有带 FlowChart 注解的才可以有这个绑定节点的菜单
     * */
    @Override
    public void update(@NotNull final AnActionEvent e) {
        try{
            PsiElement data2 = e.getData(LangDataKeys.PASTE_TARGET_PSI_ELEMENT);
            if(data2 instanceof PsiMethodImpl){
                PsiMethodImpl curMethod = (PsiMethodImpl)data2;
                PsiAnnotation[] annotations = curMethod.getAnnotations();
                /* 判断是否有生成流程图的注解 */
                Boolean enable = false;
                for(PsiAnnotation annotation:annotations){
                    PsiAnnotationImpl annotationImpl = (PsiAnnotationImpl)annotation;
                    if(MAIN_ANNOTIONS_NAME.equals(annotationImpl.getQualifiedName())|| AuthorityEnum.CHILD_ANNOTIONS_NAME.equals(annotationImpl.getQualifiedName())){
                        /* 是声明的流程图注解 */
                        enable = true;
                        break;
                    }
                }
                e.getPresentation().setVisible(enable);
            }
        }catch (Exception exce){
            e.getPresentation().setEnabled(false);
        }
    }
}
