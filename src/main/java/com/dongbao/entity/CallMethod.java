package com.dongbao.entity;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.impl.source.PsiMethodImpl;
import com.intellij.psi.impl.source.tree.java.PsiMethodCallExpressionImpl;

public class CallMethod {

    private PsiMethodCallExpressionImpl mothodCall;

    private PsiMethod method;

    private PsiClass psiClass;

    public PsiMethodCallExpressionImpl getMothodCall() {
        return mothodCall;
    }

    public void setMothodCall(PsiMethodCallExpressionImpl mothodCall) {
        this.mothodCall = mothodCall;
    }

    public PsiMethod getMethod() {
        return method;
    }

    public void setMethod(PsiMethod method) {
        this.method = method;
    }

    public PsiClass getPsiClass() {
        return psiClass;
    }

    public void setPsiClass(PsiClass psiClass) {
        this.psiClass = psiClass;
    }
}
