package com.dongbao.core.config;

import com.dongbao.dialog.SettingsForm;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.options.SearchableConfigurable;
import com.intellij.openapi.project.ProjectManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class FlowChartConfig implements SearchableConfigurable {

//    private SettingsForm frame;
    private SettingsForm frame;

    private Boolean modified;

    @NotNull
    @Override
    public String getId() {
        return "FlowChatConfig";
    }

    /**
     * 控制在配置面板中左侧窗口的显示名称
     * */
//    @Nls(capitalization =  Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        return "设置面板";
    }

    /**
     * 配置界面的绘制，负责用户输入信息的接受。
     * */
    @Nullable
    @Override
    public JComponent createComponent() {
        frame = new SettingsForm();
        frame.initPlan();
        return frame;
    }

    /**
     * 当用户修改配置参数后，在点击“OK”“Apply”按钮前，框架会自动调用该方法，判断是否有修改，进而控制按钮“OK”“Apply”的是否可用。
     * */
    @Override
    public boolean isModified() {
        if(frame == null){
            return true;
        }
        return frame.isModified.booleanValue();
    }

    @Override
    public void apply() throws ConfigurationException {
        frame.apply();
    }
}
