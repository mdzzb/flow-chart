package com.dongbao.core.config;

import com.dongbao.util.StorageConfig;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@State(name = "FlowChart",storages = {
        @Storage(value = "flow-chart.xml")
})
public class FlowChartState implements PersistentStateComponent<FlowChartState> {

    private StorageConfig config;

    public static FlowChartState getInstance(){
        return ServiceManager.getService(FlowChartState.class);
    }

    @Nullable
    @Override
    public FlowChartState getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull FlowChartState flowChartState) {
        if (null == flowChartState) {
            return;
        }
        XmlSerializerUtil.copyBean(flowChartState, this);
    }

    public StorageConfig getConfig() {
        if (null == config) {
            config = new StorageConfig();
            config.setIfPartLevel(false);
            config.setStructure("");
        }
        return config;
    }

    public void setConfig(StorageConfig config) {
        this.config = config;
    }
}