package com.dongbao.dialog;

import java.awt.*;
import javax.swing.border.*;
import com.dongbao.core.config.FlowChartState;
import com.dongbao.util.StorageConfig;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import com.intellij.uiDesigner.core.*;
import lombok.Getter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.*;

public class SettingsForm extends SimpleToolWindowPanel {

    /**
     * 是否有修改的内容
     * */
    public Boolean isModified = false;

    private final Map<String,String> structureOptions = new HashMap<String,String>(){{
        put("地图（默认）","");
        put("左头鱼骨","org.xmind.ui.fishbone.leftHeaded");
        put("右头鱼骨","org.xmind.ui.fishbone.rightHeaded");
        put("左逻辑图","org.xmind.ui.logic.left");
        put("正确的逻辑图","org.xmind.ui.logic.right");
        put("向下组织图","org.xmind.ui.org-chart.down");
        put("向上组织图","org.xmind.ui.org-chart.up");
        put("电子表格","org.xmind.ui.spreadsheet");
        put("左树","org.xmind.ui.tree.left");
        put("右树","org.xmind.ui.tree.right");
        put("浮动","org.xmind.ui.map.floating");
    }};


    public SettingsForm(boolean vertical) {
        super(vertical);
        initComponents();
    }

    public SettingsForm(boolean vertical, boolean borderless) {
        super(vertical, borderless);
        initComponents();
    }

    public SettingsForm() {
        super(true, true);
        initComponents();
    }

    public void initPlan(){
        add(rootPanel);
        /* 从存储中将值取出还原 */
        StorageConfig config = FlowChartState.getInstance().getConfig();
        if (null == config) {
            config = new StorageConfig();
        }
        //定义按钮组
        ButtonGroup group=new ButtonGroup();
        group.add(ifLevelNo);
        group.add(ifLevelYes);
        if(Optional.ofNullable(config.getIfPartLevel()).orElse(false)){
            ifLevelYes.setSelected(true);
            ifLevelNo.setSelected(false);
        }else{
            ifLevelYes.setSelected(false);
            ifLevelNo.setSelected(true);
        }

        ifLevelNo.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                isModified = true;
//                if(ifLevelNo.isSelected()){
//                    FlowChartState.getInstance().getConfig().setIfPartLevel(false);
//                }
            }
        });

        ifLevelYes.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                isModified = true;
//                if(ifLevelYes.isSelected()){
//                    FlowChartState.getInstance().getConfig().setIfPartLevel(true);
//                }
            }
        });

        //下拉选择结构组件
        String structure = Optional.ofNullable(config.getStructure()).orElse("");
        for(String name:structureOptions.keySet()){
            this.selectStructure.addItem(name);
        }
        for(String key:structureOptions.keySet()){
            if(structureOptions.get(key).equals(structure)){
                this.selectStructure.setSelectedItem(key);
            }
        }
//        this.selectStructure.setSelectedItem(structure);

        selectStructure.addItemListener(new ItemListener() {// 添加选中状态改变的监听器
            @Override
            public void itemStateChanged(ItemEvent e) {
                //SELECTED 此状态更改值指示项被选定
                //getStateChange()返回被影响的方式：去选择/选择
                if (e.SELECTED == e.getStateChange()) {
                    isModified = true;
                }
            }
        });
    }

    public void apply() {
        StorageConfig config = FlowChartState.getInstance().getConfig();
        if (null == config) {
            config = new StorageConfig();
        }
        if(ifLevelNo.isSelected()){
            FlowChartState.getInstance().getConfig().setIfPartLevel(false);
        }else if(ifLevelYes.isSelected()){
            FlowChartState.getInstance().getConfig().setIfPartLevel(true);
        }

        Object selectedItem = selectStructure.getSelectedItem();
        Map<String, String> structureOptions = this.structureOptions;
        String structure = structureOptions.get(selectedItem);
        config.setStructure(structure);

        FlowChartState.getInstance().setConfig(config);
        isModified = false;
//        Messages.showInfoMessage(config.getName(), config.getValue()+"---");
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        // Generated using JFormDesigner Evaluation license - unknown
        rootPanel = new JPanel();
        Spacer vSpacer1 = new Spacer();
        JPanel panel1 = new JPanel();
        JLabel label1 = new JLabel();
        ifLevelNo = new JRadioButton();
        ifLevelYes = new JRadioButton();
        JLabel label2 = new JLabel();
        selectStructure = new JComboBox();

        //======== rootPanel ========
        {
            rootPanel.setBorder(new TitledBorder(""));
            rootPanel.setBorder ( new CompoundBorder ( new TitledBorder ( new EmptyBorder ( 0
            , 0 ,0 , 0) ,  "JF\u006frmD\u0065sig\u006eer \u0045val\u0075ati\u006fn" , TitledBorder. CENTER , TitledBorder . BOTTOM
            , new Font ( "Dia\u006cog", Font. BOLD ,12 ) , Color .red ) ,
            rootPanel. getBorder () ) ); rootPanel. addPropertyChangeListener( new java. beans .PropertyChangeListener ( ){ @Override public void propertyChange (java . beans. PropertyChangeEvent e
            ) { if( "\u0062ord\u0065r" .equals ( e. getPropertyName () ) )throw new RuntimeException( ) ;} } );
            rootPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
            rootPanel.add(vSpacer1, new GridConstraints(1, 0, 1, 1,
                GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL,
                GridConstraints.SIZEPOLICY_CAN_SHRINK,
                GridConstraints.SIZEPOLICY_CAN_GROW | GridConstraints.SIZEPOLICY_WANT_GROW,
                null, null, null));

            //======== panel1 ========
            {
                panel1.setBorder(new TitledBorder("\u5bfc\u51fa\u914d\u7f6e"));
                panel1.setLayout(new GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));

                //---- label1 ----
                label1.setText("IF\u662f\u5426\u5206\u7ea7\uff1a");
                panel1.add(label1, new GridConstraints(0, 0, 1, 1,
                    GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
                    GridConstraints.SIZEPOLICY_FIXED,
                    GridConstraints.SIZEPOLICY_FIXED,
                    null, null, null));

                //---- ifLevelNo ----
                ifLevelNo.setSelected(false);
                ifLevelNo.setText("\u540c\u4e00\u7ea7");
                panel1.add(ifLevelNo, new GridConstraints(0, 1, 1, 1,
                    GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
                    GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                    GridConstraints.SIZEPOLICY_FIXED,
                    null, null, null));

                //---- ifLevelYes ----
                ifLevelYes.setText("\u4e0b\u4e00\u7ea7");
                panel1.add(ifLevelYes, new GridConstraints(0, 2, 1, 1,
                    GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
                    GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                    GridConstraints.SIZEPOLICY_FIXED,
                    null, null, null));

                //---- label2 ----
                label2.setText("\u9009\u62e9\u5bfc\u51fa\u4e3b\u9898\uff1a");
                panel1.add(label2, new GridConstraints(1, 0, 1, 1,
                    GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE,
                    GridConstraints.SIZEPOLICY_FIXED,
                    GridConstraints.SIZEPOLICY_FIXED,
                    null, null, null));
                panel1.add(selectStructure, new GridConstraints(1, 1, 1, 1,
                    GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL,
                    GridConstraints.SIZEPOLICY_CAN_GROW,
                    GridConstraints.SIZEPOLICY_FIXED,
                    null, null, null));
            }
            rootPanel.add(panel1, new GridConstraints(0, 0, 1, 1,
                GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW,
                null, null, null));
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    // Generated using JFormDesigner Evaluation license - unknown
    private JPanel rootPanel;
    private JRadioButton ifLevelNo;
    private JRadioButton ifLevelYes;
    private JComboBox selectStructure;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
