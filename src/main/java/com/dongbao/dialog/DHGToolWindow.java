package com.dongbao.dialog;

import com.dongbao.util.IdeaApiUtil;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import com.intellij.openapi.wm.ToolWindow;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;

public class DHGToolWindow  extends SimpleToolWindowPanel {

    private static final Logger LOGGER = Logger.getInstance(DHGToolWindow.class);

    private JPanel panel;
    private JPanel writeRulePlan;
    private JPanel codeErrorPlan;
    private JTabbedPane mainTab;
    private JTable codeErrorTable;
    private JButton export;
    private JButton openUrlBtn;

    private Project myProject;

    private final Object[] columnNames = {"类名","方法名","扣除分值","问题点","问题点2","描述信息","扫描类型"};

    public DHGToolWindow(Project project, ToolWindow toolWindow) {
        super(true, false);
        this.setContent(panel);
        this.myProject = project;
        /* 渲染错误数据展示的表格 */
        ;
        Object[][] rowData = {
//                {"ddd", "男", "江苏南京", "1378313210", "03/24/1985", "学生", "寄生中", "未婚", "没"},
//                {"eee", "女", "江苏南京", "13645181705", "xx/xx/1985", "家教", "未知", "未婚", "好象没"},
//                {"fff", "男", "江苏南京", "13585331486", "12/08/1985", "汽车推销员", "不确定", "未婚", "有"},
//                {"ggg", "女", "江苏南京", "81513779", "xx/xx/1986", "宾馆服务员", "确定但未知", "未婚", "有"},
//                {"hhh", "男", "江苏南京", "13651545936", "xx/xx/1985", "学生", "流放中", "未婚", "无数次分手后没有"}
        };
        TableModel dataModel = new DefaultTableModel(rowData,columnNames);
        codeErrorTable.setModel(dataModel);
        codeErrorTable.setEnabled(false);
        codeErrorTable.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent e){
                if(e.getClickCount() == 1){
                    //实现双击 {
                    int row =((JTable)e.getSource()).rowAtPoint(e.getPoint()); //获得行位置
                    int  col=((JTable)e.getSource()).columnAtPoint(e.getPoint());
                    //获得列位置
                    TableModel tbModel = codeErrorTable.getModel();
//                    Object cellVal=(String)(tbModel.getValueAt(row,col));
                    // 获得点击单元格数据
                }
            }
        });
        openUrlBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 调用电脑浏览器打开DHG代码规范的页面
                String url = "http://192.168.2.220/java/hcm/db-file/blob/develop/%E8%A7%84%E8%8C%83/%E4%BB%A3%E7%A0%81%E7%BC%96%E5%86%99%E8%A7%84%E8%8C%83/%E4%BB%A3%E7%A0%81%E7%BC%96%E5%86%99%E8%A7%84%E8%8C%83V1.0.12-210528_1409.md";
                try {
                    Runtime.getRuntime().exec("cmd /c start " + url);//启用cmd运行默认浏览器
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                LOGGER.info("调用电脑浏览器打开DHG代码规范的页面");
            }
        });
    }

    public DHGToolWindow(boolean vertical) {
        super(vertical);
    }

    public DHGToolWindow(boolean vertical, boolean borderless) {
        super(vertical, borderless);
    }


}
