<!-- Keep a Changelog guide -> https://keepachangelog.com -->

# flow-chart Changelog

## [Unreleased]
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security
## [1.1.4]
### Added
- 2021-11-08 : Plugin upload IDEA plugin library submission(插件上传IDEA插件库提交)
- 2021-11-10 : 增加代码提交打分功能
